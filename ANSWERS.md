
1. Describe Middleware, Sessions (as we know them in express), bcrypt and JWT.
    Middleware intercepts requests, does something to them and then passes them along
    Sessions are a persistent way to store data
    bcrypt hashes passwords (rather than encrypting them)
    JWT stands for JSON Web Token, and these tokens store login info client-side for authentication

2. What does bcrypt do in order to prevent attacks?
     bcrypt hashes passwords. This is a one-way process - once a password is hashed it cannot be recovered. This is different from encryption, which can be reversed.

3. What are the three parts of the JSON Web Token?
     header, payload, signature
