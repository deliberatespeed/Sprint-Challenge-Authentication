import React, { Component } from 'react';
import axios from 'axios';

class Register extends Component {
  state = {
    username:'',
    password:'',
  };

  render() {
    return (
      <form>
	<div>
	  <label>Username</label>
	  <input
	    value={this.state.username}
	    onChange={this.handleChange}
	    name='username'
	    type='text'/>
	</div>
	<div>
	  <label>Password</label>
	  <input
	    value={this.state.password}
	    onChange={this.handleChange}
	    name='password'
	    type='password'
	    />
	</div>
	<div>
	  <button onClick={this.handleSubmitRegister}>Register</button>
	</div>
      </form>
    );
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmitRegister = (event) => {
    event.preventDefault();
    axios
      .post('http://localhost:5000/api/users', this.state)
      .then(response => {
	localStorage.setItem('jwt', response.data.token);
	this.props.history.push('/signin');
      })
      .catch(err => console.log('wrong!'));
  };
}

export default Register;
