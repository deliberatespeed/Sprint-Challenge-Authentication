import React, { Component } from 'react';
import axios from 'axios';

class Jokes extends Component {
  state = {
    jokes: [],
  };

  render() {
    return (
      <ul>
	{this.state.jokes.map(joke => <li key={joke._id}>{joke.setup}<br/>{joke.punchline}</li>)}
      </ul>
    );
  }

  componentDidMount() {
    const token = localStorage.getItem('jwt');
    const requestOptions = {
      headers: {
	Authorization: token,
      },
    };

    axios.get('http://localhost:5000/api/jokes', requestOptions)
      .then(response => {
	this.setState({ jokes: response.data });
      })
      .catch(err => {
	console.log(err);
      });
  };

};

export default Jokes;
